//
//  TimeThingy.swift
//  kvo_swift_4
//
//  Created by Troels Michael Trebbien on 06/09/2018.
//  Copyright © 2018 momondo. All rights reserved.
//

import UIKit

class TimeThingy: NSObject {

    private var timer: Timer?
    var isTimerOn: Bool {
        return timer?.isValid ?? false
    }
    @objc var currentTime = Date() {
        willSet {
            willChangeValue(for: \.currentTime)
        }
        didSet {
            didChangeValue(for: \.currentTime)
        }
    }

    override init() {
        super.init()
    }

    func startTimer() {
        guard !isTimerOn else {
            return
        }
        timer = Timer.init(timeInterval: 1, repeats: true, block: { [unowned self] timer in
            self.currentTime = Date()
        })
        if let timer = timer {
            RunLoop.current.add(timer, forMode: .defaultRunLoopMode)
        }
    }

    func stopTimer() {
        timer?.invalidate()
    }
}
