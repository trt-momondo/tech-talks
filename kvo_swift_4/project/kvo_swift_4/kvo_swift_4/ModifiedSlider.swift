//
//  ModifiedSlider.swift
//  kvo_swift_4
//
//  Created by Troels Michael Trebbien on 06/09/2018.
//  Copyright © 2018 momondo. All rights reserved.
//

import UIKit

class ModifiedSlider: UISlider {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addTarget(self, action: #selector(valueChanged(_:)), for: .valueChanged)
    }

    @objc private func valueChanged(_ sender: UISlider) {
        willChangeValue(for: \ModifiedSlider.value)
        didChangeValue(for: \ModifiedSlider.value)
    }
}
