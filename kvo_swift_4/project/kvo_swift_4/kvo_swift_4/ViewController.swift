// For more info see: https://developer.apple.com/documentation/swift/cocoa_design_patterns/using_key_value_observing_in_swift

import UIKit

class ViewController: UIViewController {

    @IBOutlet private var resultLabel: UILabel!

    @IBOutlet private var segmentedControl: UISegmentedControl!
    private var segmentedControlObservation: NSKeyValueObservation!

    @IBOutlet private var slider: UISlider!
    private var sliderObservation: NSKeyValueObservation!

    @IBOutlet private var button: UIButton!
    private var buttonObservation: NSKeyValueObservation!

    @IBOutlet private var modifiedSlider: ModifiedSlider!
    private var modifiedSliderObservation: NSKeyValueObservation!

    private let timeThingy = TimeThingy()
    private var timeThingyObservation: NSKeyValueObservation!

    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .medium
        return formatter
    }()
    @IBOutlet private var timeLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        segmentedControlObservation = segmentedControl.observe(\.selectedSegmentIndex, options: [.initial, .new]) { [unowned self] segmentedControl, change in
            var text = "Segmented control triggered - but by what?"
            if let newValue = change.newValue, let title = segmentedControl.titleForSegment(at: newValue) {
                text = "Selected segment:\n\(title)"
                switch newValue {
                case 0:
                    self.timeThingy.stopTimer()
                    self.timeLabel.text = "What time is it?"
                default:
                    self.timeThingy.startTimer()
                }
            }
            self.resultLabel.text = text
        }

        sliderObservation = slider.observe(\.value, options: [.old, .new], changeHandler: { [unowned self] slider, change in
            var text = "Unmodified slider triggered - but by what?"
            if let newValue = change.newValue, let oldValue = change.oldValue {
                text = "Unmodified slider value:\n\(newValue)\nOld value was:\n\(oldValue)"
            }
            self.resultLabel.text = text
        })

        buttonObservation = button.observe(\.isHighlighted, options: [.new], changeHandler: { [unowned self] button, change in
            var text = "Button triggered - but by what?"
            if let newValue = change.newValue {
                text = "Button is:\n\(newValue ? "highlighted" : "lowlighted (😎)")"
            }
            self.resultLabel.text = text
        })

        modifiedSliderObservation = modifiedSlider.observe(\.value, options: [.new], changeHandler: { [unowned self] slider, change in
            var text = "Modified slider triggered - but by what?"
            if let newValue = change.newValue {
                text = "Modified slider value: \(newValue)"
            }
            self.resultLabel.text = text
        })

        timeThingyObservation = timeThingy.observe(\.currentTime, options: [], changeHandler: { [unowned self] timeThingy, _ in
            self.timeLabel.text = "The time is:\n\(self.dateFormatter.string(from: timeThingy.currentTime))"
        })

    }

}

