/*:
 [⬅️](@previous) | [➡️](@next)
 ****
 # DispatchGroup

 *DispatchGroup allows for aggregate synchronization of work. You can use them to submit multiple different work items and track when they all complete, even though they might run on different queues. This behavior can be helpful when progress can’t be made until all of the specified tasks are complete.*

 ### Apple's documentation:
 * [DispatchGroup](https://developer.apple.com/documentation/dispatch/dispatchgroup#)

 */

import Foundation

let xmasStrings = [
    "Merry Xmas!",
    "... Ya filthy animal!",
    "And a happy new year!",
]

/*:
 Without DispatchGroup:
 */

print(Santa.emoji + " says (WITHOUT DispatchGroup):")

DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 2) {
    print(xmasStrings[1])
}

DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 1) {
    print(xmasStrings[0])
}

print(xmasStrings[2])

/*:
 With DispatchGroup:
 */

print("\n" + Santa.emoji + " says (WITH DispatchGroup):")
let dispatchGroup = DispatchGroup()

dispatchGroup.enter()
DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 2) {
    print(xmasStrings[1])
    dispatchGroup.leave()
}

dispatchGroup.enter()
DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 1) {
    print(xmasStrings[0])
    dispatchGroup.leave()
}

dispatchGroup.notify(queue: .main) {
    print(xmasStrings[2])
}
