/*:
 [⬅️](@previous) | [➡️](@next)
 ****
 # The reduce function.
 When to use reduce? When you just need one value from a collection of values. That's pretty broad but it also means that reduce can be used to construct
 alternatives to functions like filter, map, first, contains etc.

 ### Apple's documentation and NSHipster:
 * [reduce(_:_:)](https://developer.apple.com/documentation/swift/array/2298686-reduce)
 * [Swift Operators](https://nshipster.com/swift-operators/)

 */
import Foundation

let randomNumbers = (1...100).map({ _ in return UInt.random(in: 0...10) })
print(randomNumbers)

/*
 Simple uses.
 */
let howManyGreaterThanSeven = randomNumbers.reduce(0) { result, number -> UInt in
    return result + (number > 7 ? 1 : 0)
}
let howManyInstancesOfThree = randomNumbers.reduce(0) { result, number -> UInt in
    return result + (number == 3 ? 1 : 0)
}

/*
 Implementing other functions with reduce (plz don't do diz).
 */
let filterWithReduce = randomNumbers.reduce([]) { result, number -> [UInt] in
    return number == 3 ? result + [number] : result
}
let mapWithReduce = randomNumbers.reduce([]) { result, number -> [UInt] in
    return result + [number + 1]
}
let firstWithReduce = randomNumbers.reduce(nil) { result, number -> UInt? in
    return result == nil && number == 3 ? number : result
}
let containsWithReduce = randomNumbers.reduce(false) { result, number -> Bool in
    return result || number == 3
}

/*
 If the signature fits...
 */
let sumOfAll = randomNumbers.reduce(0, +)
let smallestElement = randomNumbers.reduce(UInt.max, min)
let mapToFloats = randomNumbers.map(Float.init)

infix operator ~ : AdditionPrecedence
func ~ (lhs: UInt, rhs: UInt) -> UInt {
    return lhs + rhs
}
func prefixTilde(lhs: UInt, rhs: UInt) -> UInt {
    return lhs + rhs
}

let a: UInt = 4
let b: UInt = 7
let c = a ~ b
let d = prefixTilde(lhs: a, rhs: b)
let sumOfAllInfix = randomNumbers.reduce(0, ~)
let sumOfAllPrefix = randomNumbers.reduce(0, prefixTilde)
