/*:
 [⬅️](@previous) | [➡️](@next)
 ****
 # Combine
 
 ### Inspiration by Jon Sundell:
 * [Combining values with functions](https://github.com/johnsundell/swifttips#100-combining-values-with-functions)
 * [Using @autoclosure when designing Swift APIs](https://www.swiftbysundell.com/posts/using-autoclosure-when-designing-swift-api)
 */
import Foundation

/*:
 Inconvenience we're trying to solve:
 */
class InconvenientSanta: Santa {
    override init() {
        super.init()
        deliveryHandler = { [weak self] in
            guard let self = self else {
                return
            }
            self.deliverPresents(to: self.niceNotNaughtyChildren)
        }
    }
}

/*:
 Combine value with function:
 */

func combine<A, B>(_ value: A, with closure: @escaping (A) -> B) -> () -> B {
    return { closure(value) }
}

class ConvenientSanta: Santa {
    convenience init(niceChildren: [String]) {
        self.init()
        niceNotNaughtyChildren = niceChildren
        deliveryHandler = combine(self.niceNotNaughtyChildren, with: deliverPresents)
    }
}

/*:
 The shorthand in action:
 */
let convenientSanta = ConvenientSanta(niceChildren: ["Aleksi", "Zahid", "Alexia", "Valentin"])
convenientSanta.niceNotNaughtyChildren.removeLast()
convenientSanta.deliveryHandler?()

/*:
 Combine tuple value with function:
 */

let combineTupleClosure = combine((4, 7), with: +)
combineTupleClosure()

/*:
 Combine value with function using deferred evaluation:
 */

func deferredCombine<A, B>(_ valueClosure: @escaping @autoclosure () -> A, with closure: @escaping (A) -> B) -> () -> B {
    return { closure(valueClosure()) }
}

class SuperConvenientSanta: Santa {
    convenience init(niceChildren: [String]) {
        self.init()
        niceNotNaughtyChildren = niceChildren
        deliveryHandler = deferredCombine(self.niceNotNaughtyChildren, with: deliverPresents)
    }
}

let superConvenientSanta = SuperConvenientSanta(niceChildren: ["Aleksi", "Zahid", "Alexia", "Valentin"])
superConvenientSanta.niceNotNaughtyChildren.removeLast()
superConvenientSanta.deliveryHandler?()

