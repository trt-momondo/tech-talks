import Foundation

open class Santa {
    public var niceNotNaughtyChildren: [String] = []
    public var deliveryHandler: (() -> Void)?
    public static let emoji = "🎅🏼"

    public init() { }

    public func deliverPresents(to children: [String]) {
        print("\n")
        children.forEach { child in
            print(Santa.emoji + " brings lovely presents to: \(child)")
        }
    }
}
