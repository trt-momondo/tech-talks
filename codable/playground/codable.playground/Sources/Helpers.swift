import Foundation

public func readJSONData(fromFileWithName filename: String) -> Data? {
    guard let fileURL = Bundle.main.url(forResource: filename, withExtension: "json") else { return nil }
    guard let fileContents = try? String(contentsOf: fileURL, encoding: String.Encoding.utf8) else { return nil }
    return fileContents.data(using: .utf8) ?? nil
}

public func printJSONData(data: Data) {
    print("JSON file contents:\(separator)")
    guard let contents = String(data: data, encoding: .utf8) else {
        print("<Unable to read JSON data>")
        return
    }
    print(contents)
}

public let separator: String = {
    let dashes = Array(repeating: "-", count: 40).joined()
    return "\n\(dashes)\n"
}()
