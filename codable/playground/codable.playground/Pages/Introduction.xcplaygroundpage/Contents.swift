/*:
 [➡️](@next)
 ****
 # Introduction
 ## Codable Features
 * Convert Swift types into and out of external representations, e.g. JSON or Property Lists/XML.
 * Requires no third-party libraries.
 * Offers out-of-box conversion for data structures similar to their external representations.
 * Custom encoding/decoding by use of publicly declared protocols.
 * Custom handling of common data types like dates, floats and base64.
 * Configurable user info dictionaries for encoders/decoders - reuse for different tasks.
 * Many types (from Swift Standard Library, Foundation, CoreGraphics etc.) already conform to it.
 
 ## Most Important Parts
 * __Codable (Composed Protocol):__
     * __Decodable (Protocol):__
        A type that can decode itself from an external representation.
     * __Encodable (Protocol):__
        A type that can encode itself to an external representation.
 * __CodingKey (Protocol):__
    A type that can be used as a key for encoding and decoding.
 * __Containers:__
    * __Keyed containers (e.g. JSON objects).__
    * __Unkeyed containers (e.g. JSON arrays).__
    * __Single value containers.__
 */
/*:
 ****
 [➡️](@next)
 */
