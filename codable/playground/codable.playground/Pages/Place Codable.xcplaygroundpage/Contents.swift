/*:
 [⬅️](@previous) | [➡️](@next)
 ****
 # Place Codable
 */
import Foundation

let json = readJSONData(fromFileWithName: "places")!
/*:
 ### The Place class:
 */
class Place: Codable {

    struct Legacy: Codable {
        let flightId: String
        let carIds: [UInt]
        let hotelId: String
    }
    struct Link: Codable {
        let rel: String
        let href: String
    }
    struct Names: Codable {
        let locale: String
        let `default`: Names.Default
        
        struct Default: Codable {
            var variants: Names.Default.Variants
            var strings: Names.Default.Strings

            struct Variants: Codable {
                var `default`: String
            }
            struct Strings: Codable {
                var to: String
                var from: String
                var `in`: String
                var at: String
            }
        }
    }
    enum PlaceType: String, Codable {
        // Primary API types
        case continent
        case country
        case city
        case neighborhood
        case region
        case airport
        case poi
        
        // Secondary API types
        case administrative
        case beach
        case commune
        case county
        case district
        case island
        case nationalpark
        case peninsula
        case province
        case resort
        case skiresort
        case state
        case suburb
        case subway
        case touristregion
        case village
    }

    private enum CodingKeys: String, CodingKey {
        case id
        case names
        case types
        case slug
        case legacy
        case links = "_links"
    }
    
    var id: UInt
    var names: Place.Names
    var types: [Place.PlaceType]
    var slug: String
    var legacy: Place.Legacy
    var links: [Place.Link]
}
/*:
 ### Decoding and encoding Place:
 */
let decoder = JSONDecoder()
let encoder = JSONEncoder()
encoder.outputFormatting = .prettyPrinted
do {
    // Decoding
    let places = try decoder.decode([Place].self, from: json)
    let indent = ""
    
    places.forEach({ place in
        print("\(indent)Place:")
        
        let indent = indent + "\t"

        print("\(indent)ID: \(place.id)")
        print("\(indent)Slug: \(place.slug)")
        print("\(indent)Default name: \(place.names.default.variants.default)")
        print("\(indent)Legacy: \(place.legacy)")

        print("\(indent)Links:")
        place.links.forEach({ (link) in
            let indent = indent + "\t"
            print("\(indent)\(link)")
        })
        
        print("\(indent)Types:")
        place.types.forEach({ (type) in
            let indent = indent + "\t"
            print("\(indent)\(type)")
        })
    })
   
    // Encoding
    print(separator)
    let placesJSON = try encoder.encode(places)
    printJSONData(data: placesJSON)
}
catch {
    print(error)
}

/*:
 ****
 [⬅️](@previous) | [➡️](@next)
 */
