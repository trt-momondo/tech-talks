/*:
 [⬅️](@previous)
 ****
 # Further reading
 * [Codable - Swift Standard Library](https://developer.apple.com/documentation/swift/codable)
 * [Ultimate Guide to JSON Parsing with Swift 4](https://benscheirman.com/2017/06/swift-json/)
 * [Swift 4 Decodable: Beyond The Basics](https://medium.com/swiftly-swift/swift-4-decodable-beyond-the-basics-990cc48b7375)
 * [bwhiteley/JSONShootout - Compare several Swift JSON mappers](https://github.com/bwhiteley/JSONShootout)
 * [SUPER IMPORTANT TECH TALK PLANNING PAGE!!1!](https://wiki.runwaynine.com/display/MIS/Tech+Talk+Planning)
 */
